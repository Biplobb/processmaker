<?php

  // include base peer class
  require_once 'classes/om/BasePmtReportsPeer.php';

  // include object class
  include_once 'classes/PmtReports.php';


/**
 * Skeleton subclass for performing query and update operations on the 'PMT_REPORTS' table.
 *
 * 
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 * @package    classes
 */
class PmtReportsPeer extends BasePmtReportsPeer {

} // PmtReportsPeer
