<?php
/**
 * class.SdAdminDaspatch.pmFunctions.php
 *
 * ProcessMaker Open Source Edition
 * Copyright (C) 2004 - 2008 Colosa Inc.
 * *
 */

////////////////////////////////////////////////////
// SdAdminDaspatch PM Functions
//
// Copyright (C) 2007 COLOSA
//
// License: LGPL, see LICENSE
////////////////////////////////////////////////////

function SdAdminDaspatch_getMyCurrentDate()
{
	return G::CurDate('Y-m-d');
}

function SdAdminDaspatch_getMyCurrentTime()
{
	return G::CurDate('H:i:s');
}
function deleteCases_getCaseStatus($caseId) {
    $c = new Cases();
    $aInfo = $c->loadCase($caseId);
    return $aInfo['APP_STATUS'];
}
