<?php
/**
 * class.plugin.php
 *  
 */

  class pluginClass extends PMPlugin {
    function __construct() {
      set_include_path(
        PATH_PLUGINS . 'plugin' . PATH_SEPARATOR .
        get_include_path()
      );
    }

    function setup()
    {
    }

    function getFieldsForPageSetup()
    {
    }

    function updateFieldsForPageSetup()
    {
    }

  }
?>