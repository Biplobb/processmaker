<?php
/**
 * class.plugin1.pmFunctions.php
 *
 * ProcessMaker Open Source Edition
 * Copyright (C) 2004 - 2008 Colosa Inc.
 * *
 */

////////////////////////////////////////////////////
// plugin1 PM Functions
//
// Copyright (C) 2007 COLOSA
//
// License: LGPL, see LICENSE
////////////////////////////////////////////////////

function plugin1_getMyCurrentDate()
{
	return G::CurDate('Y-m-d');
}

function plugin1_getMyCurrentTime()
{
	return G::CurDate('H:i:s');
}
