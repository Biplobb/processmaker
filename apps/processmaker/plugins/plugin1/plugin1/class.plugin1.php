<?php
/**
 * class.plugin1.php
 *  
 */

  class plugin1Class extends PMPlugin {
    function __construct() {
      set_include_path(
        PATH_PLUGINS . 'plugin1' . PATH_SEPARATOR .
        get_include_path()
      );
    }

    function setup()
    {
    }

    function getFieldsForPageSetup()
    {
    }

    function updateFieldsForPageSetup()
    {
    }

  }
?>