<?php
/**
 * class.documentaco.pmFunctions.php
 *
 * ProcessMaker Open Source Edition
 * Copyright (C) 2004 - 2008 Colosa Inc.
 * *
 */

////////////////////////////////////////////////////
// documentaco PM Functions
//
// Copyright (C) 2007 COLOSA
//
// License: LGPL, see LICENSE
////////////////////////////////////////////////////

function documentaco_getMyCurrentDate()
{
	return G::CurDate('Y-m-d');
}

function documentaco_getMyCurrentTime()
{
	return G::CurDate('H:i:s');
}
