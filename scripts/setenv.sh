#!/bin/sh
echo $PATH | egrep "/home/biplob/processmaker/common" > /dev/null
if [ $? -ne 0 ] ; then
PATH="/home/biplob/processmaker/sqlite/bin:/home/biplob/processmaker/php/bin:/home/biplob/processmaker/mysql/bin:/home/biplob/processmaker/apache2/bin:/home/biplob/processmaker/common/bin:$PATH"
export PATH
fi
echo $LD_LIBRARY_PATH | egrep "/home/biplob/processmaker/common" > /dev/null
if [ $? -ne 0 ] ; then
LD_LIBRARY_PATH="/home/biplob/processmaker/sqlite/lib:/home/biplob/processmaker/mysql/lib:/home/biplob/processmaker/apache2/lib:/home/biplob/processmaker/common/lib${LD_LIBRARY_PATH:+:$LD_LIBRARY_PATH}"
export LD_LIBRARY_PATH
fi

TERMINFO=/home/biplob/processmaker/common/share/terminfo
export TERMINFO
##### SQLITE ENV #####
			
SASL_CONF_PATH=/home/biplob/processmaker/common/etc
export SASL_CONF_PATH
SASL_PATH=/home/biplob/processmaker/common/lib/sasl2 
export SASL_PATH
LDAPCONF=/home/biplob/processmaker/common/etc/openldap/ldap.conf
export LDAPCONF
##### GHOSTSCRIPT ENV #####
GS_LIB="/home/biplob/processmaker/common/share/ghostscript/fonts"
export GS_LIB
##### IMAGEMAGICK ENV #####
MAGICK_HOME="/home/biplob/processmaker/common"
export MAGICK_HOME

MAGICK_CONFIGURE_PATH="/home/biplob/processmaker/common/lib/ImageMagick-6.9.8/config-Q16:/home/biplob/processmaker/common/"
export MAGICK_CONFIGURE_PATH

MAGICK_CODER_MODULE_PATH="/home/biplob/processmaker/common/lib/ImageMagick-6.9.8/modules-Q16/coders"
export MAGICK_CODER_MODULE_PATH

##### FONTCONFIG ENV #####
FONTCONFIG_PATH="/home/biplob/processmaker/common/etc/fonts"
export FONTCONFIG_PATH
##### PHP ENV #####
PHP_PATH=/home/biplob/processmaker/php/bin/php
COMPOSER_HOME=/home/biplob/processmaker/php/composer
export PHP_PATH
export COMPOSER_HOME
##### MYSQL ENV #####

##### APACHE ENV #####

##### CURL ENV #####
CURL_CA_BUNDLE=/home/biplob/processmaker/common/openssl/certs/curl-ca-bundle.crt
export CURL_CA_BUNDLE
##### SSL ENV #####
SSL_CERT_FILE=/home/biplob/processmaker/common/openssl/certs/curl-ca-bundle.crt
export SSL_CERT_FILE
OPENSSL_CONF=/home/biplob/processmaker/common/openssl/openssl.cnf
export OPENSSL_CONF
OPENSSL_ENGINES=/home/biplob/processmaker/common/lib/engines
export OPENSSL_ENGINES


. /home/biplob/processmaker/scripts/build-setenv.sh
